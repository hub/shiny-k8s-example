# Branch "main" - Project shiny-k8s-example

This project is a satellite project containing example(s) on how to use components developped in [hub/shiny-k8s](https://gitlab.pasteur.fr/hub/shiny-k8s).

- The base example with R language is the branch [base-r](https://gitlab.pasteur.fr/hub/shiny-k8s-example/-/tree/base-r?ref_type=heads)
- The base example with Python is the branch [base-python](https://gitlab.pasteur.fr/hub/shiny-k8s-example/-/tree/base-python?ref_type=heads)

Don't use this branch directly.\
Please go to [base-r](https://gitlab.pasteur.fr/hub/shiny-k8s-example/-/tree/base-r?ref_type=heads) or [base-python](https://gitlab.pasteur.fr/hub/shiny-k8s-example/-/tree/base-python?ref_type=heads) branches according to your language used in your Shiny App.

For more information, see [Documentation](https://hub.pages.pasteur.fr/shiny-k8s/).

# Developpers

The branch [base-common](https://gitlab.pasteur.fr/hub/shiny-k8s-example/-/tree/base-common?ref_type=heads) contains material common to [base-r](https://gitlab.pasteur.fr/hub/shiny-k8s-example/-/tree/base-r?ref_type=heads) and [base-python](https://gitlab.pasteur.fr/hub/shiny-k8s-example/-/tree/base-python?ref_type=heads) branches.

# Scripts

Usefull scripts for different categories of users can be found in branch [advanced-scripts](https://gitlab.pasteur.fr/hub/shiny-k8s-example/-/tree/advanced-scripts).

# Doc-trigger

Specific orphan branch to trigger the rebuild of the documentation (list of all versions available)

# Project structure

This project use orphan branch concept [^1] to manage the different ressources using according to the usage.

```ini
shiny-k8s-example
  ├── main
  ├── base-common
  │   ├── base-r
  │   └── base-python
  ├── advanced-scripts
  └── doc-trigger
```

[^1]: [git definition](https://git-scm.com/docs/git-checkout/2.14.6#Documentation/git-checkout.txt---orphanltnewbranchgt)
